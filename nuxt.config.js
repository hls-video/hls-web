import { ProvidePlugin } from 'webpack'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  ssr: false,
  static: {
    prefix: false
  },
  head: {
    title: 'EMX',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      {
        src: '/vendor/jquery/jquery.min.js',
        type: 'text/javascript'
      },
      {
        src: '/vendor/jquery-easing/jquery.easing.min.js',
        type: 'text/javascript'
      },
      {
        src: '/vendor/bootstrap/js/bootstrap.bundle.min.js',
        type: 'text/javascript'
      },
      {
        src: '/js/sb-admin-2.min.js',
        type: 'text/javascript'
      },
      {
        src: '/vendor/sweetalert2/sweetalert2.min.js',
        type: 'text/javascript'
      },
      {
        src: '/vendor/datatables/jquery.dataTables.min.js',
        type: 'text/javascript'
      },
      {
        src: '/vendor/datatables/dataTables.bootstrap4.min.js',
        type: 'text/javascript'
      },
      {
        src: '/js/demo/datatables-demo.js',
        type: 'text/javascript'
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/fonts/nunito.css',
    '~/assets/css/sb-admin-2.min.css',
    '~/assets/vendor/fontawesome-free/css/all.min.css',
    '~/assets/vendor/datatables/dataTables.bootstrap4.min.css',
    '~/assets/vendor/sweetalert2/sweetalert2.min.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/persistedState.js' }
    // { src: '~/assets/vendor/jquery/jquery.min.js', mode: 'client' },
    // { src: '~/plugins/jquery', mode: 'client' },
    // { src: '~/assets/vendor/jquery-easing/jquery.easing.min.js', mode: 'client' },
    // { src: '~/assets/vendor/bootstrap/js/bootstrap.bundle.min.js', mode: 'client' },
    // { src: '~/assets/js/sb-admin-2.min.js', mode: 'client' }
    // { src: '~/assets/vendor/sweetalert2/sweetalert2.min.js', mode: 'client' }
    // { src: '~/assets/vendor/datatables/jquery.dataTables.min.js', mode: 'client' }
    // { src: '~/assets/vendor/datatables/dataTables.bootstrap4.min.js', mode: 'client' }
    // { src: '~/assets/js/demo/datatables-demo.js', mode: 'client' }

  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    plugins: [
      new ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
      })
    ]

  }
}
