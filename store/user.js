import { make } from 'vuex-pathify'
export const state = () => ({
  userInfo: {}
})

export const getters = make.getters(state)

export const mutations = make.mutations(state)

export const actions = {
  ...make.actions(state)
}
